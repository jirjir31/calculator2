package org.afpa.sigfox.example.JunitTest;

import java.io.IOException;

public class IntLengthException extends RuntimeException {
	
    public IntLengthException(){
        super("Length number does not match");
    }

}
