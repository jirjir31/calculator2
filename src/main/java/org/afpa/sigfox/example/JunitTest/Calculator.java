package org.afpa.sigfox.example.JunitTest;


public class Calculator {

    public int additionner(int a, int b) throws IntLengthException {
        //        System.out.println(Integer.MAX_VALUE); //2147483647
        //        System.out.println(Integer.MAX_VALUE + 3); //-2147483646
        //        System.out.println((long)Integer.MAX_VALUE+3); //2147483650
        if((long)a+b > Integer.MAX_VALUE || (long)a+b < Integer.MIN_VALUE){
            throw new IntLengthException();
        }
        return a + b;
    }

    public double diviser(int a, int b) throws DivideByZeroException {
        if(b == 0 ){
            throw new DivideByZeroException();
        }else{
            return (double)a/b;
        }
    }


    public int soustraction(int a, int b) throws IntLengthException{
        if((long)a-b > Integer.MAX_VALUE || (long)a-b < Integer.MIN_VALUE){
            throw new IntLengthException();
        }else{
            return a-b;
        }
    }


    public int multiplier(int a, int b) throws IntLengthException {
        if ((long) a * b > Integer.MAX_VALUE || (long) a * b < Integer.MIN_VALUE) {
            throw new IntLengthException();
        } else {
            return a * b;
        }
    }


}
