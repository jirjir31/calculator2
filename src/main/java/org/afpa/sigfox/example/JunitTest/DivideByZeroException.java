package org.afpa.sigfox.example.JunitTest;

public class DivideByZeroException extends RuntimeException {
	
	public DivideByZeroException(){
        super("Division par zero impossible");
    }

}
