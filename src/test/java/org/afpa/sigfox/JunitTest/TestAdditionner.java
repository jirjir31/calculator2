package org.afpa.sigfox.JunitTest;

import org.afpa.sigfox.example.JunitTest.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("dev")
@Tag("prod") // Tag possible pour classe et methode
public class TestAdditionner {

	Calculator calculatrice = new Calculator();

	@Test
	@DisplayName("test pour la methode additionner")
	// @RepeatedTest(2) //mais execute au total 1+2=3 fois
	@RepeatedTest(value = 2, name = "{displayName}, repetition {currentRepetition} sur {totalRepetitions}")
	void TestAddition() throws IntLengthException {
		System.out.println("additionTest");

		Assertions.assertEquals(7, calculatrice.additionner(3, 4), "3+4=7");
		Assertions.assertEquals(-7, calculatrice.additionner(-5, -2), "-5-2=-7");
		Assertions.assertEquals(-6, calculatrice.additionner(3, -9), "3-9=-6");
	}

	@Test
	@Tag("exception")
	void TestAdditionException1() {
		Assertions.assertThrows(IntLengthException.class, () -> calculatrice.additionner(Integer.MAX_VALUE, 3));
		Assertions.assertThrows(IntLengthException.class, () -> calculatrice.additionner(Integer.MIN_VALUE, -2));
	}

	@BeforeAll
	static void init() {
		System.out.println("BeforeAll Annotation test");
	}

	@AfterAll
	static void clearUpAll() {
		System.out.println("AfterAll Annotation test");
	}

	@BeforeEach
	void eachInit() {
		System.out.println("BeforeEach Annotation test");
	}

	@AfterEach
	void clearUp() {
		System.out.println("AfterEach Annotation test");
	}

}
