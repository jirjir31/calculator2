package org.afpa.sigfox.JunitTest;

import org.afpa.sigfox.example.JunitTest.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("prod")
class TestSoustraire {

	Calculator calculatrice = new Calculator();

	@Test
	void testSoustraction() {
		Assertions.assertEquals(3, calculatrice.soustraction(9, 6), "9-6=3");
		Assertions.assertEquals(-1, calculatrice.soustraction(-2, -1), "-2-(-1)=-1");
	}

	@Test
	@Tag("exception")
	void testSoustractionException() {
		Assertions.assertThrows(IntLengthException.class, () -> calculatrice.soustraction(Integer.MIN_VALUE, 2));
		Assertions.assertThrows(IntLengthException.class, () -> calculatrice.soustraction(Integer.MAX_VALUE, -2));
	}
}
