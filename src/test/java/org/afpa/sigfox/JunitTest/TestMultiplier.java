/**
 * 
 */
package org.afpa.sigfox.JunitTest;

import org.afpa.sigfox.example.JunitTest.Calculator;
import org.afpa.sigfox.example.JunitTest.IntLengthException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * @author Robin
 *
 */
@Tag("prod")
public class TestMultiplier {
	
	Calculator calculatrice = new Calculator();
	
	 @Test
	    void testMultiplication(){
	        Assertions.assertEquals(18, calculatrice.multiplier(3,6), "3*6=18");
	        Assertions.assertTrue(-18 == calculatrice.multiplier(-3, 6), "-3*6=-18");
	        Assertions.assertTrue(0 == calculatrice.multiplier(0, 6), "0*6=0");

	    }

	    @Test
	    @Tag("exception")
	    void testMultiplicationException(){
	        Assertions.assertThrows(IntLengthException.class, ()->calculatrice.multiplier(Integer.MAX_VALUE, 2));
	        Assertions.assertThrows(IntLengthException.class, ()->calculatrice.multiplier(Integer.MIN_VALUE, 2));
	    }
	
	

}
