/**
 * 
 */
package org.afpa.sigfox.JunitTest;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.ExcludeTags;
import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;


@RunWith(JUnitPlatform.class)
@SelectPackages("org.example")
@IncludeTags("prod")
//sans  ExcludeTags("dev") => TestAdditionner sera executé
//avec  ExcludeTags("dev") => TestAdditionner ne sera pas executé
@ExcludeTags("dev")
public class ProdTest {

}
