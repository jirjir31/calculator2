/**
 * 
 */
package org.afpa.sigfox.JunitTest;

import org.afpa.sigfox.example.JunitTest.Calculator;
import org.afpa.sigfox.example.JunitTest.DivideByZeroException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * @author Robin
 *
 */
public class TestDiviser {

	Calculator calculatrice = new Calculator();

    @Test
    void divisionTest(){

        Assertions.assertEquals(3, calculatrice.diviser(12,4), "12/4=3");
    }

   // @Disabled //valable pour methode et classe
    @Test
    @Tag("exception")
    void divisionParZeroTest(){

        Assertions.assertThrows(DivideByZeroException.class, ()->calculatrice.diviser(2,0));
    }
}
